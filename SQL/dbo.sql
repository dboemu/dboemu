/*
Navicat MySQL Data Transfer

Source Server         : home ubuntu
Source Server Version : 50534
Source Host           : 192.168.178.23:3306
Source Database       : dbo

Target Server Type    : MYSQL
Target Server Version : 50534
File Encoding         : 65001

Date: 2014-06-19 17:39:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `accounts`
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `AccountID` int(10) NOT NULL AUTO_INCREMENT,
  `Username` varchar(20) DEFAULT NULL,
  `AccountPW` varchar(100) DEFAULT NULL,
  `acc_status` varchar(7) DEFAULT 'active',
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`AccountID`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO `accounts` VALUES ('154', 'Daneos', 'lmaoxd', 'active', null);
INSERT INTO `accounts` VALUES ('160', 'Daneos2', 'lmaoxd', 'active', null);

-- ----------------------------
-- Table structure for `characters`
-- ----------------------------
DROP TABLE IF EXISTS `characters`;
CREATE TABLE `characters` (
  `CharID` int(10) NOT NULL AUTO_INCREMENT,
  `CharName` varchar(20) NOT NULL,
  `AccountID` int(10) DEFAULT NULL,
  `Level` tinyint(3) DEFAULT '1',
  `Exp` int(10) DEFAULT '0',
  `MaxExpInThisLevel` int(10) DEFAULT '0',
  `Race` tinyint(1) DEFAULT NULL,
  `Class` tinyint(2) DEFAULT NULL,
  `ChangeClass` bit(1) DEFAULT b'0',
  `Gender` tinyint(1) DEFAULT NULL,
  `Face` tinyint(2) DEFAULT NULL,
  `Adult` bit(1) DEFAULT b'0',
  `Hair` tinyint(2) DEFAULT NULL,
  `HairColor` tinyint(2) DEFAULT NULL,
  `SkinColor` tinyint(2) DEFAULT NULL,
  `CurLocX` float(10,5) DEFAULT '4583.00000',
  `CurLocY` float(10,5) DEFAULT '0.00000',
  `CurLocZ` float(10,5) DEFAULT '4070.00000',
  `CurDirX` float(10,5) DEFAULT '0.00000',
  `CurDirY` float(10,5) DEFAULT '0.00000',
  `CurDirZ` float(10,5) DEFAULT '1.00000',
  `WorldTable` int(10) DEFAULT '1',
  `WorldID` int(10) DEFAULT '1',
  `Money` int(10) DEFAULT '0',
  `MoneyBank` int(10) DEFAULT '0',
  `MapInfoIndex` int(4) DEFAULT '1',
  `TutorialFlag` bit(1) DEFAULT b'0',
  `NameChange` bit(1) DEFAULT b'0',
  `Reputation` int(10) DEFAULT '0',
  `MudosaPoint` int(10) DEFAULT '0',
  `SpPoint` int(2) DEFAULT '0',
  `GameMaster` bit(1) DEFAULT b'1',
  `GuildID` int(4) DEFAULT '0',
  `BaseStr` int(3) DEFAULT '1',
  `LastStr` int(3) DEFAULT '1',
  `BaseCon` int(3) DEFAULT '1',
  `LastCon` int(3) DEFAULT '1',
  `BaseFoc` int(3) DEFAULT '1',
  `LastFoc` int(3) DEFAULT '1',
  `BaseDex` int(3) DEFAULT '1',
  `LastDex` int(3) DEFAULT '1',
  `BaseSol` int(3) DEFAULT '1',
  `LastSol` int(3) DEFAULT '1',
  `BaseEng` int(3) DEFAULT '1',
  `LastEng` int(3) DEFAULT '1',
  `BaseMaxLP` int(5) DEFAULT '0',
  `LastMaxLP` int(5) DEFAULT '0',
  `BaseMaxEP` int(5) DEFAULT '0',
  `LastMaxEP` int(5) DEFAULT '0',
  `BaseMaxRP` int(5) DEFAULT '0',
  `LastMaxRP` int(5) DEFAULT '0',
  `BaseLpRegen` int(5) DEFAULT '0',
  `LastLpRegen` int(5) DEFAULT '0',
  `BaseLpSitdownRegen` int(5) DEFAULT '0',
  `LastLpSitdownRegen` int(5) DEFAULT '0',
  `BaseLpBattleRegen` int(5) DEFAULT '0',
  `LastLpBattleRegen` int(5) DEFAULT '0',
  `BaseEpRegen` int(5) DEFAULT '0',
  `LastEpRegen` int(5) DEFAULT '0',
  `BaseEpSitdownRegen` int(5) DEFAULT '0',
  `LastEpSitdownRegen` int(5) DEFAULT '0',
  `BaseEpBattleRegen` int(5) DEFAULT '0',
  `LastEpBattleRegen` int(5) DEFAULT '0',
  `BaseRpRegen` int(5) DEFAULT '0',
  `LastRpRegen` int(5) DEFAULT '0',
  `LastRpDimimutionRate` int(5) DEFAULT '0',
  `BasePhysicalOffence` int(5) DEFAULT '0',
  `LastPhysicalOffence` int(5) DEFAULT '0',
  `BasePhysicalDefence` int(5) DEFAULT '0',
  `LastPhysicalDefence` int(5) DEFAULT '0',
  `BaseEnergyOffence` int(5) DEFAULT '0',
  `LastEnergyOffence` int(5) DEFAULT '0',
  `BaseEnergyDefence` int(5) DEFAULT '0',
  `LastEnergyDefence` int(5) DEFAULT '0',
  `BaseAttackRate` int(5) DEFAULT '0',
  `LastAttackRate` int(5) DEFAULT '0',
  `BaseDodgeRate` int(5) DEFAULT '0',
  `LastDodgeRate` int(5) DEFAULT '0',
  `BaseBlockRate` int(5) DEFAULT '0',
  `LastBlockRate` int(5) DEFAULT '0',
  `BaseCurseSuccessRate` int(5) DEFAULT '0',
  `LastCurseSuccessRate` int(5) DEFAULT '0',
  `BaseCurseToleranceRate` int(5) DEFAULT '0',
  `LastCurseToleranceRate` int(5) DEFAULT '0',
  `BasePhysicalCriticalRate` int(5) DEFAULT '0',
  `LastPhysicalCriticalRate` int(5) DEFAULT '0',
  `BaseEnergyCriticalRate` int(5) DEFAULT '0',
  `LastEnergyCriticalRate` int(5) DEFAULT '0',
  `LastRunSpeed` float(10,5) DEFAULT '8.00000',
  `BaseAttackSpeedRate` int(5) DEFAULT '0',
  `LastAttackSpeedRate` int(5) DEFAULT '0',
  `BaseAttackRange` float(10,5) DEFAULT '0.00000',
  `LastAttackRange` float(10,5) DEFAULT '0.00000',
  `CastingTimeChangePercent` float(10,5) DEFAULT '0.00000',
  `CoolTimeChangePercent` float(10,5) DEFAULT '0.00000',
  `KeepTimeChangePercent` float(10,5) DEFAULT '0.00000',
  `DotValueChangePercent` float(10,5) DEFAULT '0.00000',
  `DotTimeChangeAbsolute` float(10,5) DEFAULT '0.00000',
  `RequiredEpChangePercent` float(10,5) DEFAULT '0.00000',
  `HonestOffence` float(10,5) DEFAULT '0.00000',
  `HonestDefence` float(10,5) DEFAULT '0.00000',
  `StrangeOffence` float(10,5) DEFAULT '0.00000',
  `StrangeDefence` float(10,5) DEFAULT '0.00000',
  `WildOffence` float(10,5) DEFAULT '0.00000',
  `WildDefence` float(10,5) DEFAULT '0.00000',
  `EleganceOffence` float(10,5) DEFAULT '0.00000',
  `EleganceDefence` float(10,5) DEFAULT '0.00000',
  `FunnyOffence` float(10,5) DEFAULT '0.00000',
  `FunnyDefence` float(10,5) DEFAULT '0.00000',
  `ParalyzeToleranceRate` int(5) DEFAULT '0',
  `TerrorToleranceRate` int(5) DEFAULT '0',
  `ConfuseToleranceRate` int(5) DEFAULT '0',
  `StoneToleranceRate` int(5) DEFAULT '0',
  `CandyToleranceRate` int(5) DEFAULT '0',
  `ParalyzeKeepTimeDown` float(10,5) DEFAULT '0.00000',
  `TerrorKeepTimeDown` float(10,5) DEFAULT '0.00000',
  `ConfuseKeepTimeDown` float(10,5) DEFAULT '0.00000',
  `StoneKeepTimeDown` float(10,5) DEFAULT '0.00000',
  `CandyKeepTimeDown` float(10,5) DEFAULT '0.00000',
  `BleedingKeepTimeDown` float(10,5) DEFAULT '0.00000',
  `PoisonKeepTimeDown` float(10,5) DEFAULT '0.00000',
  `StomachacheKeepTimeDown` float(10,5) DEFAULT '0.00000',
  `CriticalBlockSuccessRate` float(10,5) DEFAULT '0.00000',
  `GuardRate` int(5) DEFAULT '0',
  `SkillDamageBlockModeSuccessRate` float(10,5) DEFAULT '0.00000',
  `CurseBlockModeSuccessRate` float(10,5) DEFAULT '0.00000',
  `KnockdownBlockModeSuccessRate` float(10,5) DEFAULT '0.00000',
  `HtbBlockModeSuccessRate` float(10,5) DEFAULT '0.00000',
  `SitDownLpRegenBonusRate` float(10,5) DEFAULT '0.00000',
  `SitDownEpRegenBonusRate` float(10,5) DEFAULT '0.00000',
  `PhysicalCriticalDamageBonusRate` float(10,5) DEFAULT '0.00000',
  `EnergyCriticalDamageBonusRate` float(10,5) DEFAULT '0.00000',
  `ItemUpgradeBonusRate` float(10,5) DEFAULT '0.00000',
  `ItemUpgradeBreakBonusRate` float(10,5) DEFAULT '0.00000',
  `CurLP` int(10) DEFAULT '0',
  `CurEP` int(10) DEFAULT '0',
  `CurRP` int(10) DEFAULT '0',
  PRIMARY KEY (`CharID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of characters
-- ----------------------------
INSERT INTO `characters` VALUES ('18', 'Schluha', '154', '30', '200', '250', '0', '0', '', '1', '6', '', '9', '7', '1', '4679.00000', '-69.00000', '4173.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '0', '1', '', '', '0', '0', '0', '', '0', '100', '50', '90', '40', '80', '30', '70', '20', '60', '10', '50', '1', '5000', '5000', '3000', '2000', '1000', '500', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '8.00000', '0', '0', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '0', '0', '0', '0', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '5000', '2000', '0');
INSERT INTO `characters` VALUES ('24', 'xfgdg', '154', '1', '0', '0', '1', '3', '', '2', '1', '', '9', '3', '3', '4683.00000', '-70.00000', '4166.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '0', '1', '', '', '0', '0', '0', '', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '8.00000', '0', '0', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '0', '0', '0', '0', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '0', '0');
INSERT INTO `characters` VALUES ('25', 'fotzee', '160', '1', '0', '0', '2', '5', '', '0', '1', '', '1', '1', '1', '4681.00000', '-69.00000', '4169.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '0', '1', '', '', '0', '0', '0', '', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '8.00000', '0', '0', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '0', '0', '0', '0', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '0', '0');

-- ----------------------------
-- Procedure structure for `AuthLogin`
-- ----------------------------
DROP PROCEDURE IF EXISTS `AuthLogin`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `AuthLogin`(IN m_szUserID VARCHAR(20), IN m_szUserPW VARCHAR(20), OUT m_dwAccountID INT, OUT m_nResultCode INT)
BEGIN

	DECLARE dec_pw VARCHAR(100);
	DECLARE dec_status VARCHAR(7);

		IF(SELECT EXISTS(SELECT 1 FROM accounts WHERE Username = m_szUserID)) THEN
				SELECT AccountID,AccountPW,acc_status INTO m_dwAccountID,dec_pw,dec_status
				FROM accounts 
				WHERE Username = m_szUserID;
				
					IF (dec_pw = m_szUserPW) THEN
							SET m_nResultCode = 100;
					ELSE
							SET m_nResultCode = 107;
					END IF;
					IF (dec_status = 'block') THEN
							SET m_nResultCode = 113;
					END IF;

		ELSE
			SET m_nResultCode = 108;
		END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `ChangeCharname`
-- ----------------------------
DROP PROCEDURE IF EXISTS `ChangeCharname`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `ChangeCharname`(IN awchCharName VARCHAR(16), IN char_Id INT, OUT wResultCode INT)
BEGIN

		IF(SELECT EXISTS(SELECT 1 FROM characters WHERE CharName = awchCharName)) THEN

			SET wResultCode = 205;

		ELSE
				
				IF(awchCharName = "GM" || awchCharName = "GameMaster" || awchCharName = "Admin" || awchCharName = "Support") THEN

					SET wResultCode = 205;

				ELSE

					UPDATE characters SET CharName = awchCharName, NameChange = 0 WHERE CharID = char_Id LIMIT 1;

					SET wResultCode = 200;

				END IF;

		END IF;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `CharCreate`
-- ----------------------------
DROP PROCEDURE IF EXISTS `CharCreate`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `CharCreate`(IN awchCharName VARCHAR(16), IN byRace INT, IN byClass INT, IN byGender INT, IN byFace INT, IN byHair INT, IN byHairColor INT, IN bySkinColor INT, IN account_id INT, OUT char_id INT, OUT wResultCode INT)
BEGIN

		IF(SELECT EXISTS(SELECT 1 FROM characters WHERE CharName = awchCharName)) THEN

			SET wResultCode = 205;

		ELSE

				IF(awchCharName = "GM" || awchCharName = "GameMaster" || awchCharName = "Admin" || awchCharName = "Support") THEN

					SET wResultCode = 205;

				ELSE

					INSERT INTO characters (CharName,AccountID,Race,Class,Gender,Face,Hair,HairColor,SkinColor) 
					VALUES 
					(awchCharName,account_id,byRace,byClass,byGender,byFace,byHair,byHairColor,bySkinColor);
				
					SET char_id = LAST_INSERT_ID();
					SET wResultCode = 200;

				END IF;

		END IF;

END
;;
DELIMITER ;
