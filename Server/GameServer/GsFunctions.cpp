#include "stdafx.h"
#include "GameServer.h"


sPC_SUMMARY		CClientSession::GetPcSummary(sPC_SUMMARY * sPcDataSummary, int characterID, CGameServer * app)
{

	app->db->prepare("SELECT * FROM characters WHERE CharID = ?");
	app->db->setInt(1, characterID);
	app->db->execute();

	while(app->db->fetch()) {
	sPcDataSummary->bIsAdult = app->db->getBoolean("Adult");
	sPcDataSummary->bNeedNameChange = app->db->getBoolean("NameChange");
	sPcDataSummary->bTutorialFlag = app->db->getBoolean("TutorialFlag");
	sPcDataSummary->byClass = app->db->getInt("Class");
	sPcDataSummary->byFace = app->db->getInt("Face");
	sPcDataSummary->byGender = app->db->getInt("Gender");
	sPcDataSummary->byHair = app->db->getInt("Hair");
	sPcDataSummary->byHairColor = app->db->getInt("HairColor");
	sPcDataSummary->byLevel = app->db->getInt("Level");
	sPcDataSummary->byRace = app->db->getInt("Race");
	sPcDataSummary->bySkinColor = app->db->getInt("SkinColor");
	sPcDataSummary->charId = app->db->getInt("CharID");
	sPcDataSummary->dwMapInfoIndex = app->db->getInt("MapInfoIndex");
	sPcDataSummary->dwMoney = app->db->getInt("Money");
	sPcDataSummary->dwMoneyBank = app->db->getInt("MoneyBank");
	sPcDataSummary->fPositionX = (float)app->db->getDouble("CurLocX");
	sPcDataSummary->fPositionY = (float)app->db->getDouble("CurLocY");
	sPcDataSummary->fPositionZ = (float)app->db->getDouble("CurLocZ");
	sPcDataSummary->sDogi.byDojoColor = 0;
	sPcDataSummary->sDogi.byGuildColor = 0;
	sPcDataSummary->sDogi.byType = 0;
	sPcDataSummary->sDogi.guildId = 0;
	//sPcDataSummary->sItem;
	sPcDataSummary->sMarking.byCode = 0; //= res->getString("sMarking");
	sPcDataSummary->worldId = app->db->getInt("WorldID");
	sPcDataSummary->worldTblidx = app->db->getInt("WorldTable");
	}

	return *sPcDataSummary;
}

sPC_BRIEF		CClientSession::GetPcBrief(sPC_BRIEF * sPcBriefSummary, int characterID, CGameServer * app)
{

	app->db->prepare("SELECT * FROM characters WHERE CharID = ?");
	app->db->setInt(1, characterID);
	app->db->execute();
	app->db->fetch();

	sPcBriefSummary->charId = app->db->getInt("CharID");
	sPcBriefSummary->tblidx = app->db->getInt("WorldTable");
	sPcBriefSummary->bIsAdult = app->db->getBoolean("Adult");
	wcscpy_s(sPcBriefSummary->awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(app->db->getString("CharName")).c_str() );
	//GET GUILD

	//
	sPcBriefSummary->sPcShape.byFace = app->db->getInt("Face");
	sPcBriefSummary->sPcShape.byHair = app->db->getInt("Hair");
	sPcBriefSummary->sPcShape.byHairColor = app->db->getInt("HairColor");
	sPcBriefSummary->sPcShape.bySkinColor = app->db->getInt("SkinColor");
	sPcBriefSummary->wCurLP = app->db->getInt("CurLP");
	sPcBriefSummary->wMaxLP = app->db->getInt("BaseMaxLP");
	sPcBriefSummary->wCurEP = app->db->getInt("CurEP");
	sPcBriefSummary->wMaxEP = app->db->getInt("BaseMaxEP");
	sPcBriefSummary->byLevel = app->db->getInt("Level");
	sPcBriefSummary->fSpeed = (float)app->db->getDouble("LastRunSpeed");
	//LOAD ITEMS WHICH CHARACTERS WEAR

	//
	sPcBriefSummary->wAttackSpeedRate = app->db->getInt("BaseAttackSpeedRate");
	//LOAD DOGI (guild)

	//

	return *sPcBriefSummary;
}

sPC_SHAPE		CClientSession::GetPcShape(sPC_SHAPE * sPcShapeSummary, int characterID, CGameServer * app)
{

	app->db->prepare("SELECT Face,Hair,HairColor,SkinColor FROM characters WHERE CharID = ?");
	app->db->setInt(1, characterID);
	app->db->execute();
	app->db->fetch();

	sPcShapeSummary->byFace = app->db->getInt("Face");
	sPcShapeSummary->byHair = app->db->getInt("Hair");
	sPcShapeSummary->byHairColor = app->db->getInt("HairColor");
	sPcShapeSummary->bySkinColor = app->db->getInt("SkinColor");


	return *sPcShapeSummary;
}

