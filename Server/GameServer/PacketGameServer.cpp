#include "stdafx.h"
#include "NtlTokenizer.h"

#include "NtlPacketTU.h"
#include "NtlPacketUT.h"
#include "GameServer.h"

RwUInt32 AcquireNPCSerialId(void)
{
	if(uiNPCSerialId++)
	{
		if(uiNPCSerialId == 0xffffffff)//INVALID_SERIAL_ID)
			uiNPCSerialId = 0;
	}

	return uiNPCSerialId;
}


RwUInt32 AcquireSerialId(void)
{
	if(m_uiSerialId++)
	{
		if(m_uiSerialId == 0xffffffff)//INVALID_SERIAL_ID)
			m_uiSerialId = 0;
	}

	return m_uiSerialId;
}
RwUInt32 AcquireTargetSerialId(void)
{
	if(uiTargetSerialId++)
	{
		if(uiTargetSerialId == 0xffffffff)//INVALID_SERIAL_ID)
			uiTargetSerialId = 0;
	}

	return uiTargetSerialId;
}
//--------------------------------------------------------------------------------------//
//		Log into Game Server
//--------------------------------------------------------------------------------------//
void CClientSession::SendGameEnterReq(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- LOAD CHAT SERVER --- \n");
	sUG_GAME_ENTER_REQ * req = (sUG_GAME_ENTER_REQ *)pPacket->GetPacketData();

	this->accountID = req->accountId;
	this->characterID = req->charId;

	CNtlPacket packet(sizeof(sGU_GAME_ENTER_RES));
	sGU_GAME_ENTER_RES * res = (sGU_GAME_ENTER_RES *)packet.GetPacketData();

	res->wOpCode = GU_GAME_ENTER_RES;
	res->wResultCode = GAME_SUCCESS;
	strcpy_s(res->achCommunityServerIP, sizeof(res->achCommunityServerIP), "192.168.1.41");
	res->wCommunityServerPort = 20400;

	packet.SetPacketLen( sizeof(sGU_GAME_ENTER_RES) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );
}
//--------------------------------------------------------------------------------------//
//		Send avatar char info
//--------------------------------------------------------------------------------------//
void CClientSession::SendAvatarCharInfo(CNtlPacket * pPacket, CGameServer * app)
{
	avatarHandle = AcquireSerialId();

	printf("--- LOAD CHARACTER INFO FOR GAMESERVER --- \n");
	CNtlPacket packet(sizeof(sGU_AVATAR_CHAR_INFO));
	sGU_AVATAR_CHAR_INFO * res = (sGU_AVATAR_CHAR_INFO *)packet.GetPacketData();

	app->db->prepare("UPDATE characters SET OnlineID = ? WHERE CharID = ?");
	app->db->setInt(1, avatarHandle);
	app->db->setInt(2, this->GetCharacterId());
	app->db->execute();

	app->db->prepare("SELECT * FROM characters WHERE CharID = ?");
	app->db->setInt(1, this->GetCharacterId());
	app->db->execute();
	app->db->fetch();

	this->charName = app->db->getString("CharName");

	CPCTable *pPcTable = app->g_pTableContainer->GetPcTable();
	sPC_TBLDAT *pTblData = (sPC_TBLDAT*)pPcTable->GetPcTbldat(app->db->getInt("Race"),app->db->getInt("Class"),app->db->getInt("Gender"));

	
	res->wOpCode = GU_AVATAR_CHAR_INFO;
	res->handle = avatarHandle;
	res->sPcProfile.tblidx = pTblData->tblidx;
	res->sPcProfile.bIsAdult = app->db->getBoolean("Adult");
	res->sPcProfile.charId = app->db->getInt("CharID");
	wcscpy_s(res->sPcProfile.awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(app->db->getString("CharName")).c_str() );
	//PC Shape
	res->sPcProfile.sPcShape.byFace = app->db->getInt("Face");
	res->sPcProfile.sPcShape.byHair = app->db->getInt("Hair");
	res->sPcProfile.sPcShape.byHairColor = app->db->getInt("HairColor");
	res->sPcProfile.sPcShape.bySkinColor = app->db->getInt("SkinColor");
	//Character Attribute
	res->sPcProfile.avatarAttribute.byBaseStr = app->db->getInt("BaseStr");
	res->sPcProfile.avatarAttribute.byLastStr = app->db->getInt("LastStr");
	res->sPcProfile.avatarAttribute.byBaseCon = app->db->getInt("BaseCon");
	res->sPcProfile.avatarAttribute.byLastCon = app->db->getInt("LastCon");
	res->sPcProfile.avatarAttribute.byBaseFoc = app->db->getInt("BaseFoc");
	res->sPcProfile.avatarAttribute.byLastFoc = app->db->getInt("LastFoc");
	res->sPcProfile.avatarAttribute.byBaseDex = app->db->getInt("BaseDex");
	res->sPcProfile.avatarAttribute.byLastDex = app->db->getInt("LastDex");
	res->sPcProfile.avatarAttribute.byBaseSol = app->db->getInt("BaseSol");
	res->sPcProfile.avatarAttribute.byLastSol = app->db->getInt("LastSol");
	res->sPcProfile.avatarAttribute.byBaseEng = app->db->getInt("BaseEng");
	res->sPcProfile.avatarAttribute.byLastEng = app->db->getInt("LastEng");
	res->sPcProfile.avatarAttribute.wBaseMaxLP = app->db->getInt("BaseMaxLP");
	res->sPcProfile.avatarAttribute.wLastMaxLP = app->db->getInt("BaseMaxLP");
	res->sPcProfile.avatarAttribute.wBaseMaxEP = app->db->getInt("BaseMaxEP");
	res->sPcProfile.avatarAttribute.wLastMaxEP = app->db->getInt("BaseMaxEP");
	res->sPcProfile.avatarAttribute.wBaseMaxRP = app->db->getInt("BaseMaxRP");
	res->sPcProfile.avatarAttribute.wLastMaxRP = app->db->getInt("BaseMaxRP");
	res->sPcProfile.avatarAttribute.wBasePhysicalOffence = app->db->getInt("BasePhysicalOffence");
	res->sPcProfile.avatarAttribute.wLastPhysicalOffence = app->db->getInt("LastPhysicalOffence");
	res->sPcProfile.avatarAttribute.wBasePhysicalDefence = app->db->getInt("BasePhysicalDefence");
	res->sPcProfile.avatarAttribute.wLastPhysicalDefence = app->db->getInt("LastPhysicalDefence");
	res->sPcProfile.avatarAttribute.wBaseEnergyOffence = app->db->getInt("BaseEnergyOffence");
	res->sPcProfile.avatarAttribute.wLastEnergyOffence = app->db->getInt("LastEnergyOffence");
	res->sPcProfile.avatarAttribute.wBaseEnergyDefence = app->db->getInt("BaseEnergyDefence");
	res->sPcProfile.avatarAttribute.wLastEnergyDefence = app->db->getInt("LastEnergyDefence");
	res->sPcProfile.avatarAttribute.wBaseAttackRate = app->db->getInt("BaseAttackRate");
	res->sPcProfile.avatarAttribute.wLastAttackRate = app->db->getInt("LastAttackRate");
	res->sPcProfile.avatarAttribute.wBaseDodgeRate = app->db->getInt("BaseDodgeRate");
	res->sPcProfile.avatarAttribute.wLastDodgeRate = app->db->getInt("LastDodgeRate");
	res->sPcProfile.avatarAttribute.wBaseBlockRate = app->db->getInt("BaseBlockRate");
	res->sPcProfile.avatarAttribute.wLastBlockRate = app->db->getInt("LastBlockRate");
	res->sPcProfile.avatarAttribute.wLastPhysicalCriticalRate = app->db->getInt("LastPhysicalCriticalRate");
	res->sPcProfile.avatarAttribute.wLastEnergyCriticalRate = app->db->getInt("LastEnergyCriticalRate");
	res->sPcProfile.avatarAttribute.fLastRunSpeed = (float)app->db->getDouble("LastRunSpeed");
	res->sPcProfile.wCurLP = app->db->getInt("CurLP");
	res->sPcProfile.wCurEP = app->db->getInt("CurEP");
	res->sPcProfile.wCurRP = app->db->getInt("CurRP");
	res->sPcProfile.byLevel = app->db->getInt("Level");
	res->sPcProfile.dwCurExp = app->db->getInt("Exp");
	res->sPcProfile.dwMaxExpInThisLevel = app->db->getInt("MaxExpInThisLevel");
	res->sPcProfile.dwZenny = app->db->getInt("Money");
	res->sPcProfile.dwTutorialHint = 0;
	res->sPcProfile.dwReputation = app->db->getInt("Reputation");
	res->sPcProfile.dwMudosaPoint = app->db->getInt("MudosaPoint");
	res->sPcProfile.dwSpPoint = app->db->getInt("SpPoint");

	// Character State
	res->sCharState.sCharStateBase.vCurLoc.x = (float)app->db->getDouble("CurLocX");
	res->sCharState.sCharStateBase.vCurLoc.y = (float)app->db->getDouble("CurLocY");
	res->sCharState.sCharStateBase.vCurLoc.z = (float)app->db->getDouble("CurLocZ");
	res->sCharState.sCharStateBase.vCurDir.x = (float)app->db->getDouble("CurDirX");
	res->sCharState.sCharStateBase.vCurDir.y = (float)app->db->getDouble("CurDirY");
	res->sCharState.sCharStateBase.vCurDir.z = (float)app->db->getDouble("CurDirZ");
	res->sCharState.sCharStateBase.dwConditionFlag = 0;
	res->sCharState.sCharStateBase.byStateID = 0;
	res->sCharState.sCharStateBase.aspectState.sAspectStateBase.byAspectStateId = 255;
	res->sCharState.sCharStateBase.aspectState.sAspectStateDetail.sGreatNamek.bySourceGrade = 0;
	res->sCharState.sCharStateBase.aspectState.sAspectStateDetail.sKaioken.bySourceGrade = 0;
	res->sCharState.sCharStateBase.aspectState.sAspectStateDetail.sPureMajin.bySourceGrade = 0;
	res->sCharState.sCharStateBase.aspectState.sAspectStateDetail.sSuperSaiyan.bySourceGrade = 0;
	res->sCharState.sCharStateBase.aspectState.sAspectStateDetail.sVehicle.idVehicleTblidx = 0;
	res->wCharStateSize = sizeof(sCHARSTATE_BASE);

	res->sPcProfile.bIsGameMaster = app->db->getBoolean("GameMaster");


	packet.SetPacketLen( sizeof(sGU_AVATAR_CHAR_INFO) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );
}

//--------------------------------------------------------------------------------------//
//		SendAvatarInfoEnd
//--------------------------------------------------------------------------------------//
void CClientSession::SendAvatarInfoEnd(CNtlPacket * pPacket)
{
	printf("--- SendAvatarInfoEnd --- \n");
	CNtlPacket packet(sizeof(sGU_AVATAR_INFO_END));
	sGU_AVATAR_INFO_END * res = (sGU_AVATAR_INFO_END *)packet.GetPacketData();

	res->wOpCode = GU_AVATAR_INFO_END;

	packet.SetPacketLen( sizeof(sGU_AVATAR_INFO_END) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );
}

//--------------------------------------------------------------------------------------//
//		Login into World
//--------------------------------------------------------------------------------------//
void CClientSession::SendWorldEnterReq(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- sGU_AVATAR_WORLD_INFO --- \n");

	CNtlPacket packet(sizeof(sGU_AVATAR_WORLD_INFO));
	sGU_AVATAR_WORLD_INFO * res = (sGU_AVATAR_WORLD_INFO *)packet.GetPacketData();

	app->db->prepare("SELECT * FROM characters WHERE CharID = ?");
	app->db->setInt(1, this->GetCharacterId());
	app->db->execute();
	app->db->fetch();

	res->wOpCode = GU_AVATAR_WORLD_INFO;
	res->worldInfo.tblidx = app->db->getInt("WorldTable");
	res->worldInfo.worldID = app->db->getInt("WorldID");
	res->worldInfo.hTriggerObjectOffset = 100000;
	res->worldInfo.sRuleInfo.byRuleType = GAMERULE_NORMAL;
	res->vCurLoc.x = (float)app->db->getDouble("CurLocX");
	res->vCurLoc.y = (float)app->db->getDouble("CurLocY");
	res->vCurLoc.z = (float)app->db->getDouble("CurLocZ");
	res->vCurDir.x = (float)app->db->getDouble("CurDirX");  
	res->vCurDir.y = (float)app->db->getDouble("CurDirY"); 
	res->vCurDir.z = (float)app->db->getDouble("CurDirZ"); 

	packet.SetPacketLen( sizeof(sGU_AVATAR_WORLD_INFO) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );

}
//--------------------------------------------------------------------------------------//
//		Character ready request
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharReadyReq(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- sGU_OBJECT_CREATE --- \n");


//SPAWN PLAYERS
	CNtlPacket packet(sizeof(sGU_OBJECT_CREATE));
	sGU_OBJECT_CREATE * res = (sGU_OBJECT_CREATE *)packet.GetPacketData();

	app->db->prepare("SELECT * FROM characters WHERE CharID = ?");
	app->db->setInt(1, this->GetCharacterId());
	app->db->execute();
	app->db->fetch();

	CPCTable *pPcTable = app->g_pTableContainer->GetPcTable();
	sPC_TBLDAT *pTblData = (sPC_TBLDAT*)pPcTable->GetPcTbldat(app->db->getInt("Race"),app->db->getInt("Class"),app->db->getInt("Gender"));


	res->wOpCode = GU_OBJECT_CREATE;
	res->handle = this->GetavatarHandle();
	res->sObjectInfo.objType = OBJTYPE_PC;
	res->sObjectInfo.pcBrief.tblidx = pTblData->tblidx;
	res->sObjectInfo.pcBrief.bIsAdult = app->db->getBoolean("Adult");
	wcscpy_s(res->sObjectInfo.pcBrief.awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(app->db->getString("CharName")).c_str() );
	wcscpy_s(res->sObjectInfo.pcBrief.wszGuildName, NTL_MAX_SIZE_GUILD_NAME_IN_UNICODE, s2ws(app->db->getString("GuildName")).c_str() );
	res->sObjectInfo.pcBrief.sPcShape.byFace = app->db->getInt("Face");
	res->sObjectInfo.pcBrief.sPcShape.byHair = app->db->getInt("Hair");
	res->sObjectInfo.pcBrief.sPcShape.byHairColor = app->db->getInt("HairColor");
	res->sObjectInfo.pcBrief.sPcShape.bySkinColor = app->db->getInt("SkinColor");
	res->sObjectInfo.pcBrief.wCurLP = app->db->getInt("CurLP");
	res->sObjectInfo.pcBrief.wMaxLP = app->db->getInt("BaseMaxLP");
	res->sObjectInfo.pcBrief.wCurEP = app->db->getInt("CurEP");
	res->sObjectInfo.pcBrief.wMaxEP = app->db->getInt("BaseMaxEP");
	res->sObjectInfo.pcBrief.byLevel = app->db->getInt("Level");
	res->sObjectInfo.pcBrief.fSpeed = (float)app->db->getDouble("LastRunSpeed");
	//Get items which the characters is wearing
	for(int i = 0;i < NTL_MAX_EQUIP_ITEM_SLOT;i++)
	{
		res->sObjectInfo.pcBrief.sItemBrief[i].tblidx = INVALID_TBLIDX; //sAvData.sItem[i].tblidx;
	}
	res->sObjectInfo.pcBrief.wAttackSpeedRate = app->db->getInt("BaseAttackSpeedRate");
	res->sObjectInfo.pcState.sCharStateBase.vCurLoc.x = (float)app->db->getDouble("CurLocX");
	res->sObjectInfo.pcState.sCharStateBase.vCurLoc.y = (float)app->db->getDouble("CurLocY");
	res->sObjectInfo.pcState.sCharStateBase.vCurLoc.z = (float)app->db->getDouble("CurLocZ");
	res->sObjectInfo.pcState.sCharStateBase.vCurDir.x = (float)app->db->getDouble("CurDirX");  
	res->sObjectInfo.pcState.sCharStateBase.vCurDir.y = (float)app->db->getDouble("CurDirY");  
	res->sObjectInfo.pcState.sCharStateBase.vCurDir.z = (float)app->db->getDouble("CurDirZ");
	res->sObjectInfo.pcState.sCharStateBase.dwConditionFlag = 0;
	res->sObjectInfo.pcState.sCharStateBase.byStateID = 0;
	res->sObjectInfo.pcState.sCharStateBase.aspectState.sAspectStateBase.byAspectStateId = 255;
	res->sObjectInfo.pcState.sCharStateBase.aspectState.sAspectStateDetail.sGreatNamek.bySourceGrade = 0;
	res->sObjectInfo.pcState.sCharStateBase.aspectState.sAspectStateDetail.sKaioken.bySourceGrade = 0;
	res->sObjectInfo.pcState.sCharStateBase.aspectState.sAspectStateDetail.sPureMajin.bySourceGrade = 0;
	res->sObjectInfo.pcState.sCharStateBase.aspectState.sAspectStateDetail.sSuperSaiyan.bySourceGrade = 0;
	res->sObjectInfo.pcState.sCharStateBase.aspectState.sAspectStateDetail.sVehicle.idVehicleTblidx = 0;

	memcpy(&this->characterspawnInfo, res, sizeof(sGU_OBJECT_CREATE) );
	packet.SetPacketLen( sizeof(sGU_OBJECT_CREATE) );

	app->UserBroadcastothers(&packet, this);
	app->UserBroadcasFromOthers(GU_OBJECT_CREATE, this);	
	app->AddUser(app->db->getString("CharName").c_str(), this);


}

//--------------------------------------------------------------------------------------//
//		Auth community Server
//--------------------------------------------------------------------------------------//
void CClientSession::SendAuthCommunityServer(CNtlPacket * pPacket, CGameServer * app)
{

	CNtlPacket packet(sizeof(sGU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES));
	sGU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES * res = (sGU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES *)packet.GetPacketData();

	res->wOpCode = GU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES;
	res->wResultCode = GAME_SUCCESS;
	strcpy_s((char*)res->abyAuthKey, NTL_MAX_SIZE_AUTH_KEY, "ChatCon");
	packet.SetPacketLen( sizeof(sGU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );

}

//--------------------------------------------------------------------------------------//
//		Login into World
//--------------------------------------------------------------------------------------//
void CClientSession::SendNpcCreate(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- CREATE NPCS --- \n");

	app->db->prepare("SELECT WorldID FROM characters WHERE CharID = ?");
	app->db->setInt(1, this->GetCharacterId());
	app->db->execute();
	app->db->fetch();

//SPAWN ALL NPC
	CSpawnTable* pNPCSpawnTbl = app->g_pTableContainer->GetNpcSpawnTable( app->db->getInt("WorldID") );
	printf("number of npcs to spawn %i \n", pNPCSpawnTbl->GetNumberOfTables());
			
		for ( CTable::TABLEIT itNPCSpawn = pNPCSpawnTbl->Begin(); itNPCSpawn != pNPCSpawnTbl->End(); ++itNPCSpawn )
		{
			sSPAWN_TBLDAT* pNPCSpwnTblData = (sSPAWN_TBLDAT*) itNPCSpawn->second;
			sNPC_TBLDAT* pNPCTblData = (sNPC_TBLDAT*)app->g_pTableContainer->GetNpcTable()->FindData( pNPCSpwnTblData->mob_Tblidx );

			if ( pNPCTblData && pNPCTblData->dwFunc_Bit_Flag & NPC_FUNC_FLAG_QUEST_GRANTER )
			{

				CNtlPacket packet(sizeof(sGU_OBJECT_CREATE));
				sGU_OBJECT_CREATE * sPacket = (sGU_OBJECT_CREATE *)packet.GetPacketData();

				sPacket->wOpCode = GU_OBJECT_CREATE;
				sPacket->sObjectInfo.objType = OBJTYPE_NPC;
				sPacket->handle = AcquireNPCSerialId();
				sPacket->sObjectInfo.npcState.sCharStateBase.vCurLoc.x = pNPCSpwnTblData->vSpawn_Loc.x;
				sPacket->sObjectInfo.npcState.sCharStateBase.vCurLoc.y = pNPCSpwnTblData->vSpawn_Loc.y;
				sPacket->sObjectInfo.npcState.sCharStateBase.vCurLoc.z = pNPCSpwnTblData->vSpawn_Loc.z;
				sPacket->sObjectInfo.npcState.sCharStateBase.vCurDir.x = pNPCSpwnTblData->vSpawn_Dir.x;
				sPacket->sObjectInfo.npcState.sCharStateBase.vCurDir.y = pNPCSpwnTblData->vSpawn_Dir.y;
				sPacket->sObjectInfo.npcState.sCharStateBase.vCurDir.z = pNPCSpwnTblData->vSpawn_Dir.z;
				sPacket->sObjectInfo.npcState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
				sPacket->sObjectInfo.npcState.sCharStateBase.bFightMode = false;
			//	sPacket->sObjectInfo.npcState.sCharStateBase.dwConditionFlag = 0;
			//	sPacket->sObjectInfo.npcState.sCharStateBase.aspectState.sAspectStateBase.byAspectStateId = 0xff;
			//	sPacket->sObjectInfo.npcState.sCharStateBase.dwStateTime = 0;
			//	sPacket->sObjectInfo.npcBrief.actionpatternTblIdx = 0;
				sPacket->sObjectInfo.npcBrief.fLastRunningSpeed = pNPCTblData->fRun_Speed;
				sPacket->sObjectInfo.npcBrief.fLastWalkingSpeed = pNPCTblData->fWalk_Speed;
				sPacket->sObjectInfo.npcBrief.nicknameTblidx = pNPCSpwnTblData->mob_Tblidx;
				sPacket->sObjectInfo.npcBrief.wCurEP = pNPCTblData->wBasic_EP;
				sPacket->sObjectInfo.npcBrief.wCurLP = pNPCTblData->wBasic_LP;
				sPacket->sObjectInfo.npcBrief.wMaxEP = pNPCTblData->wBasic_EP;
				sPacket->sObjectInfo.npcBrief.wMaxLP = pNPCTblData->wBasic_LP;
				sPacket->sObjectInfo.npcBrief.tblidx = pNPCSpwnTblData->mob_Tblidx;

				packet.SetPacketLen( sizeof(sGU_OBJECT_CREATE) );
				g_pApp->Send( this->GetHandle(), &packet );
				
			}
		}


}

//--------------------------------------------------------------------------------------//
//		Spawn Mobs
//--------------------------------------------------------------------------------------//
void CClientSession::SendMonsterCreate(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- LOAD MONSTERS --- \n");

	app->db->prepare("SELECT WorldID FROM characters WHERE CharID = ?");
	printf("--- Query Prepared --- \n");
	app->db->setInt(1, this->GetCharacterId());
	printf("--- SetInt GetCharacterID Passed --- \n");
	app->db->execute();
	printf("--- Query Executed --- \n");
	app->db->fetch();
	printf("--- DB Fetched--- \n");

//SPAWN Mobs under level 10
	CSpawnTable* pMobSpawnTbl = app->g_pTableContainer->GetMobSpawnTable((TBLIDX)1);
	printf("number of Monsters to spawn %i \n", pMobSpawnTbl->GetNumberOfTables());
			
		for ( CTable::TABLEIT itMobSpawn = pMobSpawnTbl->Begin(); itMobSpawn != pMobSpawnTbl->End(); ++itMobSpawn )
		{
			sSPAWN_TBLDAT* pMobSpwnTblData = (sSPAWN_TBLDAT*) itMobSpawn->second;
			sMOB_TBLDAT* pMobTblData = (sMOB_TBLDAT*)app->g_pTableContainer->GetMobTable()->FindData( pMobSpwnTblData->mob_Tblidx );

			if ( pMobTblData )
			{

				CNtlPacket packet(sizeof(sGU_OBJECT_CREATE));
				sGU_OBJECT_CREATE * sPacket = (sGU_OBJECT_CREATE *)packet.GetPacketData();

				sPacket->wOpCode = GU_OBJECT_CREATE;
				sPacket->sObjectInfo.objType = OBJTYPE_MOB;
				sPacket->handle = AcquireNPCSerialId();
				sPacket->sObjectInfo.mobState.sCharStateBase.vCurLoc.x = pMobSpwnTblData->vSpawn_Loc.x;
				sPacket->sObjectInfo.mobState.sCharStateBase.vCurLoc.y = pMobSpwnTblData->vSpawn_Loc.y;
				sPacket->sObjectInfo.mobState.sCharStateBase.vCurLoc.z = pMobSpwnTblData->vSpawn_Loc.z;
				sPacket->sObjectInfo.mobState.sCharStateBase.vCurDir.x = pMobSpwnTblData->vSpawn_Dir.x;
				sPacket->sObjectInfo.mobState.sCharStateBase.vCurDir.y = pMobSpwnTblData->vSpawn_Dir.y;
				sPacket->sObjectInfo.mobState.sCharStateBase.vCurDir.z = pMobSpwnTblData->vSpawn_Dir.z;
				sPacket->sObjectInfo.mobState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
				sPacket->sObjectInfo.mobState.sCharStateBase.bFightMode = false;
			//	sPacket->sObjectInfo.npcState.sCharStateBase.dwConditionFlag = 0;
			//	sPacket->sObjectInfo.npcState.sCharStateBase.aspectState.sAspectStateBase.byAspectStateId = 0xff;
			//	sPacket->sObjectInfo.npcState.sCharStateBase.dwStateTime = 0;
			//	sPacket->sObjectInfo.npcBrief.actionpatternTblIdx = 0;
				sPacket->sObjectInfo.mobBrief.fLastRunningSpeed = pMobTblData->fRun_Speed;
				sPacket->sObjectInfo.mobBrief.fLastWalkingSpeed = pMobTblData->fWalk_Speed;
			//	sPacket->sObjectInfo.mobBrief.nicknameTblidx = pNPCSpwnTblData->mob_Tblidx;
				sPacket->sObjectInfo.mobBrief.wCurEP = pMobTblData->wBasic_EP;
				sPacket->sObjectInfo.mobBrief.wCurLP = pMobTblData->wBasic_LP;
				sPacket->sObjectInfo.mobBrief.wMaxEP = pMobTblData->wBasic_EP;
				sPacket->sObjectInfo.mobBrief.wMaxLP = pMobTblData->wBasic_LP;
				sPacket->sObjectInfo.mobBrief.tblidx = pMobSpwnTblData->mob_Tblidx;

				packet.SetPacketLen( sizeof(sGU_OBJECT_CREATE) );
				g_pApp->Send( this->GetHandle(), &packet );
				
			}
		}


}

//--------------------------------------------------------------------------------------//
//		SendEnterWorldComplete
//--------------------------------------------------------------------------------------//
void CClientSession::SendEnterWorldComplete(CNtlPacket * pPacket)
{
	printf("--- SendEnterWorldComplete --- \n");

	CNtlPacket packet(sizeof(sGU_ENTER_WORLD_COMPLETE));
	sGU_ENTER_WORLD_COMPLETE * res = (sGU_ENTER_WORLD_COMPLETE *)packet.GetPacketData();

	res->wOpCode = GU_ENTER_WORLD_COMPLETE;

	packet.SetPacketLen( sizeof(sGU_ENTER_WORLD_COMPLETE) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );
}

//--------------------------------------------------------------------------------------//
//		Tutorial Hint request
//--------------------------------------------------------------------------------------//
void CClientSession::SendTutorialHintReq(CNtlPacket * pPacket, CGameServer * app)
{
	sUG_TUTORIAL_HINT_UPDATE_REQ * req = (sUG_TUTORIAL_HINT_UPDATE_REQ *)pPacket->GetPacketData();
	//req->dwTutorialHint;
	printf("--- TUTORIAL HINT REQUEST %i --- \n", req->dwTutorialHint);

	CNtlPacket packet(sizeof(sGU_TUTORIAL_HINT_UPDATE_RES));
	sGU_TUTORIAL_HINT_UPDATE_RES * res = (sGU_TUTORIAL_HINT_UPDATE_RES *)packet.GetPacketData();

	//app->db->prepare("SELECT * FROM characters WHERE CharID = ?");
	//app->db->setInt(1, this->characterID);
	//app->db->execute();
	//app->db->fetch();
	res->wOpCode = GU_TUTORIAL_HINT_UPDATE_RES;
	res->wResultCode = GAME_SUCCESS;
	res->dwTutorialHint = 0;

	packet.SetPacketLen( sizeof(sGU_TUTORIAL_HINT_UPDATE_RES) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );
}

//--------------------------------------------------------------------------------------//
//		Char Ready
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharReady(CNtlPacket * pPacket)
{
	printf("--- SEND CHAR READY --- \n");

	CNtlPacket packet(sizeof(sUG_CHAR_READY));
	sUG_CHAR_READY * res = (sUG_CHAR_READY *)packet.GetPacketData();

	res->wOpCode = UG_CHAR_READY;
	res->byAvatarType = 0;

	packet.SetPacketLen( sizeof(sUG_CHAR_READY) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );
}

//--------------------------------------------------------------------------------------//
//		Char Move
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharMove(CNtlPacket * pPacket, CGameServer * app)
{
	//printf("--- SEND CHAR MOVE --- \n");

	sUG_CHAR_MOVE * req = (sUG_CHAR_MOVE*)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sGU_CHAR_MOVE));
	sGU_CHAR_MOVE * res = (sGU_CHAR_MOVE *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_MOVE;
	res->handle = this->GetavatarHandle();
	//res->dwTimeStamp = req->dwTimeStamp;
	res->vCurLoc.x = req->vCurLoc.x;
	res->vCurLoc.y = req->vCurLoc.y;
	res->vCurLoc.z = req->vCurLoc.z;
	res->vCurDir.x = req->vCurDir.x;
	res->vCurDir.y = 0;
	res->vCurDir.z = req->vCurDir.z;
	res->byMoveDirection = req->byMoveDirection;
	res->byMoveFlag = NTL_MOVE_FIRST;

	packet.SetPacketLen( sizeof(sGU_CHAR_MOVE) );
	app->UserBroadcastothers(&packet, this);

	app->db->prepare("UPDATE characters SET CurLocX=? , CurLocY=? , CurLocZ=? , CurDirX=? , CurDirZ=? WHERE CharID = ?");
	app->db->setInt(1, res->vCurLoc.x);
	app->db->setInt(2, res->vCurLoc.y);
	app->db->setInt(3, res->vCurLoc.z);
	app->db->setInt(4, res->vCurDir.x);
	app->db->setInt(5, res->vCurDir.z);
	app->db->setInt(6, this->characterID);
	app->db->execute();
}
//--------------------------------------------------------------------------------------//
//		Char Destination Move
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharDestMove(CNtlPacket * pPacket, CGameServer * app)
{
	//printf("--- CHARACTER REQUEST DEST MOVE --- \n");

	sUG_CHAR_DEST_MOVE * req = (sUG_CHAR_DEST_MOVE*)pPacket->GetPacketData();
	
	CNtlPacket packet(sizeof(sGU_CHAR_DEST_MOVE));
	sGU_CHAR_DEST_MOVE * res = (sGU_CHAR_DEST_MOVE *)packet.GetPacketData();
	
	res->wOpCode = GU_CHAR_DEST_MOVE;
	res->handle = this->GetavatarHandle();
	res->dwTimeStamp = req->dwTimeStamp;
	res->vCurLoc.x = req->vCurLoc.x;
	res->vCurLoc.y = req->vCurLoc.y;
	res->vCurLoc.z = req->vCurLoc.z;
	res->byMoveFlag = NTL_MOVE_MOUSE_MOVEMENT;
	res->bHaveSecondDestLoc = false;
	res->byDestLocCount = 1;
	res->avDestLoc[0].x = req->vDestLoc.x;
	res->avDestLoc[0].y = req->vDestLoc.y;
	res->avDestLoc[0].z = req->vDestLoc.z;

	packet.SetPacketLen( sizeof(sGU_CHAR_DEST_MOVE) );
	app->UserBroadcastothers(&packet, this);
	//int rc = g_pApp->Send( this->GetHandle(), &packet );
}
//--------------------------------------------------------------------------------------//
//		Char Move Sync
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharMoveSync(CNtlPacket * pPacket, CGameServer * app)
{
	//printf("--- CHARACTER MOVE SYNC --- \n");
	sUG_CHAR_MOVE_SYNC * req = (sUG_CHAR_MOVE_SYNC*)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sGU_CHAR_MOVE_SYNC));
	sGU_CHAR_MOVE_SYNC * res = (sGU_CHAR_MOVE_SYNC *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_MOVE_SYNC;
	res->handle = this->GetavatarHandle();
	res->vCurLoc.x = req->vCurLoc.x;
	res->vCurLoc.y = req->vCurLoc.y;
	res->vCurLoc.z = req->vCurLoc.z;
	res->vCurDir.x = req->vCurDir.x;
	res->vCurDir.y = req->vCurDir.y;
	res->vCurDir.z = req->vCurDir.z;

	packet.SetPacketLen( sizeof(sGU_CHAR_MOVE_SYNC) );
	app->UserBroadcastothers(&packet, this);
}
//--------------------------------------------------------------------------------------//
//		Char Change Heading
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharChangeHeading(CNtlPacket * pPacket, CGameServer * app)
{
	//printf("--- CHARACTER CHANGE HEADING --- \n");
	sUG_CHAR_CHANGE_HEADING * req = (sUG_CHAR_CHANGE_HEADING*)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sGU_CHAR_CHANGE_HEADING));
	sGU_CHAR_CHANGE_HEADING * res = (sGU_CHAR_CHANGE_HEADING *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_CHANGE_HEADING;
	res->handle = this->GetavatarHandle();
	res->vNewHeading.x = req->vCurrentHeading.x;
	res->vNewHeading.y = req->vCurrentHeading.y;
	res->vNewHeading.z = req->vCurrentHeading.z;
	res->vNewLoc.x = req->vCurrentPosition.x;
	res->vNewLoc.y = req->vCurrentPosition.y;
	res->vNewLoc.z = req->vCurrentPosition.z;

	packet.SetPacketLen( sizeof(sGU_CHAR_CHANGE_HEADING) );
	app->UserBroadcastothers(&packet, this);
}
//--------------------------------------------------------------------------------------//
//		Char Jump
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharJump(CNtlPacket * pPacket, CGameServer * app)
{
	//printf("--- CHARACTER JUMP --- \n");
	sUG_CHAR_CHANGE_HEADING * req = (sUG_CHAR_CHANGE_HEADING*)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sGU_CHAR_JUMP));
	sGU_CHAR_JUMP * res = (sGU_CHAR_JUMP *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_JUMP;
	res->handle = this->GetavatarHandle();
	res->vCurrentHeading.x = req->vCurrentHeading.x;
	res->vCurrentHeading.y = req->vCurrentHeading.y;
	res->vCurrentHeading.z = req->vCurrentHeading.z;

	res->vJumpDir.x = 0;
	res->vJumpDir.y = 0;
	res->vJumpDir.z = 0;

	res->byMoveDirection = 1;

	packet.SetPacketLen( sizeof(sGU_CHAR_JUMP) );
	app->UserBroadcastothers(&packet, this);
}
//--------------------------------------------------------------------------------------//
//		Change Char Direction on floating
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharChangeDirOnFloating(CNtlPacket * pPacket, CGameServer * app)
{
//	printf("--- Change Char Direction on floating --- \n");
	sUG_CHAR_CHANGE_DIRECTION_ON_FLOATING * req = (sUG_CHAR_CHANGE_DIRECTION_ON_FLOATING*)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sGU_CHAR_CHANGE_DIRECTION_ON_FLOATING));
	sGU_CHAR_CHANGE_DIRECTION_ON_FLOATING * res = (sGU_CHAR_CHANGE_DIRECTION_ON_FLOATING *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_CHANGE_DIRECTION_ON_FLOATING;
	res->hSubject = this->GetavatarHandle();
	res->vCurDir.x = req->vCurDir.x;
	res->vCurDir.y = req->vCurDir.y;
	res->vCurDir.z = req->vCurDir.z;
	res->byMoveDirection = req->byMoveDirection;

	packet.SetPacketLen( sizeof(sGU_CHAR_CHANGE_DIRECTION_ON_FLOATING) );
	app->UserBroadcastothers(&packet, this);
}
//--------------------------------------------------------------------------------------//
//		Char falling
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharFalling(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- character falling --- \n");
	sUG_CHAR_FALLING * req = (sUG_CHAR_FALLING*)pPacket->GetPacketData();

	req->wOpCode = UG_CHAR_FALLING;
	req->bIsFalling = true;

	req->vCurLoc.x;
	req->vCurLoc.y;
	req->vCurLoc.z;
	req->vCurDir.x;
	req->vCurDir.z;
	req->byMoveDirection;

}

//--------------------------------------------------------------------------------------//
//		GM Command
//--------------------------------------------------------------------------------------//
void CClientSession::RecvServerCommand(CNtlPacket * pPacket, CGameServer * app)
{
	sUG_SERVER_COMMAND * pServerCmd = (sUG_SERVER_COMMAND*)pPacket;

	char chBuffer[1024];
	::WideCharToMultiByte(GetACP(), 0, pServerCmd->awchCommand, -1, chBuffer, 1024, NULL, NULL);

	CNtlTokenizer lexer(chBuffer);

	if(!lexer.IsSuccess())
		return;

	enum ECmdParseState
	{
		SERVER_CMD_NONE,
		SERVER_CMD_KEY,
		SERVER_CMD_END,
	};

	ECmdParseState eState = SERVER_CMD_KEY;
	int iOldLine = 0;
	int iLine;
	
	while(1)
	{
		std::string strToken = lexer.PeekNextToken(NULL, &iLine);

		if(strToken == "") 
			break;
	
		switch (eState)
		{
		case SERVER_CMD_KEY:
			if(strToken == "@setspeed")
			{
				printf("received char speed command");
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				float fSpeed = (float)atof(strToken.c_str());
				CClientSession::SendUpdateCharSpeed(fSpeed, app);
				return;
			}
			else if(strToken == "@addmob")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int uiMobId = (unsigned int)atoi(strToken.c_str());
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				float fDist = (float)atof(strToken.c_str());
				lexer.PopToPeek();

				//SendMonsterCreate(uiMobId, fDist);
				
				return;
			}
			else if(strToken == "@addmobg")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int iNum = (unsigned int)atoi(strToken.c_str());
				//SendMonsterGroupCreate(iNum);
				return;
			}
			else if(strToken == "@createitem")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int uiTblId = (unsigned int)atof(strToken.c_str());
			//	SendAddItem(uiTblId);
				return;
			}
			else if(strToken == "@learnskill")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int uiTblId = (unsigned int)atof(strToken.c_str());
			//	SendCharLearnSkillRes(uiTblId);
				return;
			}
			else if(strToken == "@learnhtb")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				unsigned int uiTblId = (unsigned int)atof(strToken.c_str());
			//	SendCharLearnHTBRes(uiTblId);
				return;
			}
			else if(strToken == "@refreshlp")
			{
				app->db->prepare("SELECT LastMaxLp FROM characters WHERE CharID = ?");
				app->db->setInt(1, this->characterID);
				app->db->execute();
				app->db->fetch();
				int max_lp = app->db->getInt("LastMaxLp");

				return; 
			}
			else if(strToken == "@setscale")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
				float fScale = (float)atof(strToken.c_str());
			//	CNtlSob *pSobObj = GetNtlSobManager()->GetSobObject(m_uiTargetSerialId);
			//	if(pSobObj)
			//	{
			//		CNtlSobProxy *pSobProxy = pSobObj->GetSobProxy();
			//		pSobProxy->SetScale(fScale);
			//	}
				
				return;
			}
			else if(strToken == "@is")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
			//	CNtlBehaviorProjSteal::m_ffIncSpeed = (RwReal)atof(strToken.c_str());
			}
			else if(strToken == "@iw")
			{
				lexer.PopToPeek();
				strToken = lexer.PeekNextToken(NULL, &iLine);
			//	CNtlBehaviorProjSteal::m_fWaitCheckTime = (RwReal)atof(strToken.c_str());
			}
			else if(strToken == "@compilelua")
			{
		//		SLLua_Setup();
				return;
			}
			
			break;
		}

		lexer.PopToPeek();
	}
}
//--------------------------------------------------------------------------------------//
//		Update Char speed *dont work*
//--------------------------------------------------------------------------------------//
void CClientSession::SendUpdateCharSpeed(float fSpeed, CGameServer * app)
{
	printf("Update char speed");
	CNtlPacket packet(sizeof(sGU_UPDATE_CHAR_SPEED));
	sGU_UPDATE_CHAR_SPEED * res = (sGU_UPDATE_CHAR_SPEED *)packet.GetPacketData();

	res->wOpCode = GU_UPDATE_CHAR_SPEED;
	res->handle = this->GetavatarHandle();
	res->fLastWalkingSpeed = fSpeed;
	res->fLastRunningSpeed = fSpeed;

	packet.SetPacketLen( sizeof(sGU_UPDATE_CHAR_SPEED) );
	app->UserBroadcastothers(&packet, this);
}
//--------------------------------------------------------------------------------------//
//		Select target
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharTargetSelect(CNtlPacket * pPacket)
{
	printf("UG_CHAR_TARGET_SELECT");

	sUG_CHAR_TARGET_SELECT * req = (sUG_CHAR_TARGET_SELECT*)pPacket->GetPacketData();
	uiTargetSerialId = req->hTarget;
}
//--------------------------------------------------------------------------------------//
//		Select target
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharTargetFacing(CNtlPacket * pPacket)
{
	printf("UG_CHAR_TARGET_FACING");

	sUG_CHAR_TARGET_SELECT * req = (sUG_CHAR_TARGET_SELECT*)pPacket->GetPacketData();
	uiTargetSerialId = req->hTarget;
}
//--------------------------------------------------------------------------------------//
//		target info
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharTargetInfo(CNtlPacket * pPacket)
{
	printf("UG_CHAR_TARGET_INFO");
	sUG_CHAR_TARGET_SELECT * req = (sUG_CHAR_TARGET_SELECT*)pPacket->GetPacketData();

}
//--------------------------------------------------------------------------------------//
//		Send game leave request
//--------------------------------------------------------------------------------------//
void CClientSession::SendGameLeaveReq(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- CHARACTER REQUEST LEAVE GAME --- \n");

	app->RemoveUser(this->GetCharName().c_str());

	CNtlPacket packet(sizeof(sGU_OBJECT_DESTROY));
	sGU_OBJECT_DESTROY * sPacket = (sGU_OBJECT_DESTROY *)packet.GetPacketData();

	sPacket->wOpCode = GU_OBJECT_DESTROY;
	sPacket->handle = this->GetavatarHandle();
	packet.SetPacketLen( sizeof(sGU_OBJECT_DESTROY) );
	app->UserBroadcastothers(&packet, this);

}
//--------------------------------------------------------------------------------------//
//		Char exit request
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharExitReq(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- Char exit request --- \n");
// log out of game
	CNtlPacket packet1(sizeof(sGU_OBJECT_DESTROY));
	sGU_OBJECT_DESTROY * sPacket = (sGU_OBJECT_DESTROY *)packet1.GetPacketData();

	sPacket->wOpCode = GU_OBJECT_DESTROY;
	sPacket->handle = this->GetavatarHandle();
	packet1.SetPacketLen( sizeof(sGU_OBJECT_DESTROY) );
	app->UserBroadcastothers(&packet1, this);

	app->RemoveUser(this->GetCharName().c_str());

// log in to char server
	CNtlPacket packet(sizeof(sGU_CHAR_EXIT_RES));
	sGU_CHAR_EXIT_RES * res = (sGU_CHAR_EXIT_RES *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_EXIT_RES;
	res->wResultCode = GAME_SUCCESS;
	strcpy_s((char*)res->achAuthKey, NTL_MAX_SIZE_AUTH_KEY, "Dbo");
	res->byServerInfoCount = 1;
	strcpy_s(res->aServerInfo[0].szCharacterServerIP, NTL_MAX_LENGTH_OF_IP, "192.168.1.41");
	res->aServerInfo[0].wCharacterServerPortForClient = 20300;
	res->aServerInfo[0].dwLoad = 0;

	packet.SetPacketLen( sizeof(sGU_CHAR_EXIT_RES) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );
}

//--------------------------------------------------------------------------------------//
//		Char sit down
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharSitDown(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- Char sit down request --- \n");

	CNtlPacket packet(sizeof(sGU_CHAR_SITDOWN));
	sGU_CHAR_SITDOWN * sPacket = (sGU_CHAR_SITDOWN *)packet.GetPacketData();

	sPacket->wOpCode = GU_CHAR_SITDOWN;
	sPacket->handle = this->GetavatarHandle();
	packet.SetPacketLen( sizeof(sGU_CHAR_SITDOWN) );
	app->UserBroadcastothers(&packet, this);
}
//--------------------------------------------------------------------------------------//
//		Char stand up
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharStandUp(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- Char stand up request --- \n");

	CNtlPacket packet(sizeof(sGU_CHAR_STANDUP));
	sGU_CHAR_STANDUP * sPacket = (sGU_CHAR_STANDUP *)packet.GetPacketData();

	sPacket->wOpCode = GU_CHAR_STANDUP;
	sPacket->handle = this->GetavatarHandle();
	packet.SetPacketLen( sizeof(sGU_CHAR_STANDUP) );
	app->UserBroadcastothers(&packet, this);
}
//--------------------------------------------------------------------------------------//
//		Char toggle fight
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharToggleFighting(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- SEND CHAR TOGGLE FIGHT--- \n");
	/*sUG_CHAR_TOGGLE_FIGHT * req = (sUG_CHAR_FOLLOW_MOVE*)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sGU_CHAR_FOLLOW_MOVE));
	sGU_CHAR_FOLLOW_MOVE * res = (sGU_CHAR_FOLLOW_MOVE *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_FOLLOW_MOVE;
	res->handle = this->GetavatarHandle();
	res->hTarget = this->GetTargetSerialId();
	res->fDistance = req->fDistance;
	res->byMovementReason = req->byMovementReason;
	res->byMoveFlag = NTL_MOVE_FLAG_RUN;

	packet.SetPacketLen( sizeof(sGU_CHAR_FOLLOW_MOVE) );
	app->UserBroadcastothers(&packet, this);
	//int rc = g_pApp->Send( this->GetHandle(), &packet );
	*/
}

//--------------------------------------------------------------------------------------//
//		char start mail
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharMailStart(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- SEND CHAR START MAIL --- \n");
}

//--------------------------------------------------------------------------------------//
//		char follow move
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharFollowMove(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- SEND CHAR FOLLOW MOVE --- \n");
	sUG_CHAR_FOLLOW_MOVE * req = (sUG_CHAR_FOLLOW_MOVE*)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sGU_CHAR_FOLLOW_MOVE));
	sGU_CHAR_FOLLOW_MOVE * res = (sGU_CHAR_FOLLOW_MOVE *)packet.GetPacketData();

	res->wOpCode = GU_CHAR_FOLLOW_MOVE;
	res->handle = this->GetavatarHandle();
	res->hTarget = this->GetTargetSerialId();
	res->fDistance = req->fDistance;
	res->byMovementReason = req->byMovementReason;
	res->byMoveFlag = NTL_MOVE_FLAG_RUN;

	packet.SetPacketLen( sizeof(sGU_CHAR_FOLLOW_MOVE) );
	app->UserBroadcastothers(&packet, this);
	//int rc = g_pApp->Send( this->GetHandle(), &packet );
}



//--------------------------------------------------------------------------------------//
//		Create Guild
//--------------------------------------------------------------------------------------//
void CClientSession::SendGuildCreateReq(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- create guild request --- \n");

	sUG_GUILD_CREATE_REQ * req = (sUG_GUILD_CREATE_REQ*)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sGU_GUILD_CREATE_RES));
	sGU_GUILD_CREATE_RES * res = (sGU_GUILD_CREATE_RES *)packet.GetPacketData();

	res->wOpCode = GU_GUILD_CREATE_RES;
	printf("guild manager id: %i ", req->hGuildManagerNpc);

	app->db->prepare("CALL GuildCreate (?,?, @wResultCode, @cguildid, @charactername)");

	app->db->setString(1, Ntl_WC2MB(req->wszGuildName));
	app->db->setInt(2, this->characterID);

	app->db->execute();
	app->db->execute("SELECT @wResultCode, @cguildid, @charactername");
	app->db->fetch(); 

	int result = app->db->getInt("@wResultCode");

	printf("create guild result %i \n ", result);
	res->wResultCode = result;

	packet.SetPacketLen( sizeof(sGU_GUILD_CREATE_RES) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );

	if (result == 200) { 
		
// CREATE GUILD
		CNtlPacket packet2(sizeof(sTU_GUILD_CREATED_NFY));
		sTU_GUILD_CREATED_NFY * res2 = (sTU_GUILD_CREATED_NFY *)packet2.GetPacketData();
		res2->wOpCode = TU_GUILD_CREATED_NFY;
		memcpy(res2->wszGuildName, req->wszGuildName, sizeof(wchar_t)* NTL_MAX_SIZE_GUILD_NAME_IN_UNICODE);
		packet2.SetPacketLen( sizeof(sTU_GUILD_CREATED_NFY));
		rc = g_pApp->Send( this->GetHandle(), &packet2);

// GUILD INFORMATIONS
		CNtlPacket packet3(sizeof(sTU_GUILD_INFO));
		sTU_GUILD_INFO * res3 = (sTU_GUILD_INFO *)packet3.GetPacketData();

		res3->wOpCode = TU_GUILD_INFO;
		res3->guildInfo.dwGuildReputation = 0;
		res3->guildInfo.guildId = app->db->getInt("@cguildid");
		res3->guildInfo.guildMaster = this->characterID;
		memcpy(res3->guildInfo.wszName, req->wszGuildName, sizeof(wchar_t)* NTL_MAX_SIZE_GUILD_NAME_IN_UNICODE);
		wcscpy_s(res3->guildInfo.awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(app->db->getString("@charactername")).c_str());
		packet3.SetPacketLen( sizeof(sTU_GUILD_INFO));
		rc = g_pApp->Send( this->GetHandle(), &packet3);

// GUILD MEMBER INFORMATIONS
		CNtlPacket packet4(sizeof(sTU_GUILD_MEMBER_INFO));
		sTU_GUILD_MEMBER_INFO * res4 = (sTU_GUILD_MEMBER_INFO *)packet4.GetPacketData();

		res4->wOpCode = TU_GUILD_MEMBER_INFO;
		res4->guildMemberInfo.bIsOnline = true;
		res4->guildMemberInfo.charId = this->characterID;
		wcscpy_s(res4->guildMemberInfo.wszMemberName, NTL_MAX_SIZE_CHAR_NAME_UNICODE, s2ws(app->db->getString("@charactername")).c_str());
		packet4.SetPacketLen( sizeof(sTU_GUILD_MEMBER_INFO));
		rc = g_pApp->Send( this->GetHandle(), &packet4);
		app->UserBroadcastothers(&packet4, this);
		printf("char name: %s ", app->db->getString("@charactername").c_str());
		
	}
	
}












//--------------------------------------------------------------------------------------//
//		Create Party
//--------------------------------------------------------------------------------------//
void CClientSession::SendCreatePartyReq(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- create party request --- \n");

	sUG_PARTY_CREATE_REQ * req = (sUG_PARTY_CREATE_REQ*)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sGU_PARTY_CREATE_RES));
	sGU_PARTY_CREATE_RES * res = (sGU_PARTY_CREATE_RES *)packet.GetPacketData();

	res->wOpCode = GU_PARTY_CREATE_RES;
	res->wResultCode = GAME_SUCCESS;
	memcpy(res->wszPartyName, req->wszPartyName, sizeof(wchar_t)* NTL_MAX_SIZE_PARTY_NAME_IN_UNICODE);

	packet.SetPacketLen( sizeof(sGU_PARTY_CREATE_RES) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );


	CNtlPacket packet2(sizeof(sGU_PARTY_DISBANDED_NFY));
	sGU_PARTY_DISBANDED_NFY * sPacket2 = (sGU_PARTY_DISBANDED_NFY *)packet2.GetPacketData();
	sPacket2->wOpCode = GU_PARTY_DISBANDED_NFY;

	packet2.SetPacketLen( sizeof(sGU_PARTY_DISBANDED_NFY));
	rc = g_pApp->Send( this->GetHandle(), &packet2);
}
//--------------------------------------------------------------------------------------//
//		Disband Party
//--------------------------------------------------------------------------------------//
void CClientSession::SendDisbandPartyReq(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- disband party request --- \n");

	CNtlPacket packet(sizeof(sGU_PARTY_DISBAND_RES));
	sGU_PARTY_DISBAND_RES * res = (sGU_PARTY_DISBAND_RES *)packet.GetPacketData();

	res->wOpCode = GU_PARTY_DISBAND_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen( sizeof(sGU_PARTY_DISBAND_RES) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );


	CNtlPacket packet2(sizeof(sGU_PARTY_DISBANDED_NFY));
	sGU_PARTY_DISBANDED_NFY * sPacket2 = (sGU_PARTY_DISBANDED_NFY *)packet2.GetPacketData();
	sPacket2->wOpCode = GU_PARTY_DISBANDED_NFY;

	packet2.SetPacketLen( sizeof(sGU_PARTY_DISBANDED_NFY));
	rc = g_pApp->Send( this->GetHandle(), &packet2);

}
//--------------------------------------------------------------------------------------//
//		Send party invite request
//--------------------------------------------------------------------------------------//
void CClientSession::SendPartyInviteReq(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- Send party invite request --- \n");

	sUG_PARTY_INVITE_REQ * req = (sUG_PARTY_INVITE_REQ*)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sGU_PARTY_INVITE_RES));
	sGU_PARTY_INVITE_RES * res = (sGU_PARTY_INVITE_RES *)packet.GetPacketData();

	res->wOpCode = GU_PARTY_INVITE_RES;
	res->wResultCode = GAME_SUCCESS;
	wcscpy_s(res->wszTargetName, NTL_MAX_SIZE_CHAR_NAME_UNICODE, s2ws("fotzee").c_str());
	printf("target uniquee id %i ", req->hTarget);

	packet.SetPacketLen( sizeof(sGU_PARTY_INVITE_RES) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );


	printf("--- Send party invite notification --- \n");

	CNtlPacket packet2(sizeof(sGU_PARTY_INVITE_NFY));
	sGU_PARTY_INVITE_NFY * res2 = (sGU_PARTY_INVITE_NFY *)packet2.GetPacketData();

	res2->wOpCode = GU_PARTY_INVITE_NFY;
	res2->bFromPc = true;
	//memcpy(res2->wszInvitorPartyName, req->wszPartyName, sizeof(wchar_t)* NTL_MAX_SIZE_PARTY_NAME_IN_UNICODE);
	wcscpy_s(res2->wszInvitorPcName, NTL_MAX_SIZE_CHAR_NAME_UNICODE, s2ws("Schluha").c_str());

	packet2.SetPacketLen( sizeof(sGU_PARTY_INVITE_NFY));
	app->UserBroadcastothers(&packet2, this);

}
//--------------------------------------------------------------------------------------//
//		Party invitation response
//--------------------------------------------------------------------------------------//
void CClientSession::SendPartyResponse(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- Party invitation response --- \n");

	sUG_PARTY_RESPONSE_INVITATION * req = (sUG_PARTY_RESPONSE_INVITATION*)pPacket->GetPacketData();

	printf("--- RESPONSE %i --- \n", req->byResponse);

	CNtlPacket packet(sizeof(sGU_PARTY_RESPONSE_INVITATION_RES));
	sGU_PARTY_RESPONSE_INVITATION_RES * res = (sGU_PARTY_RESPONSE_INVITATION_RES *)packet.GetPacketData();

	res->wOpCode = GU_PARTY_RESPONSE_INVITATION_RES;
	res->wResultCode = GAME_SUCCESS;
	
	packet.SetPacketLen( sizeof(sGU_PARTY_RESPONSE_INVITATION_RES) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );
	


	CNtlPacket packet2(sizeof(sGU_PARTY_MEMBER_JOINED_NFY));
	sGU_PARTY_MEMBER_JOINED_NFY * res2 = (sGU_PARTY_MEMBER_JOINED_NFY *)packet2.GetPacketData();
	res2->wOpCode = GU_PARTY_MEMBER_JOINED_NFY;
	wcscpy_s(res2->memberInfo.awchMemberName, NTL_MAX_SIZE_CHAR_NAME_UNICODE, s2ws("fotzee").c_str());

	/*
	res2->memberInfo.byClass = 5;
	res2->memberInfo.byLevel = 1;
	res2->memberInfo.byRace = 2;
	res2->memberInfo.hHandle = this->GetTargetSerialId();
	res2->memberInfo.vCurLoc.x = 4605;
	res2->memberInfo.vCurLoc.y = -44;
	res2->memberInfo.vCurLoc.z = 4092;
	res2->memberInfo.wCurEP = 1000;
	res2->memberInfo.wCurLP = 1000;
	res2->memberInfo.wMaxEP = 1000;
	res2->memberInfo.wMaxLP = 1000;
	res2->memberInfo.worldId = 1;
	*/

	packet2.SetPacketLen( sizeof(sGU_PARTY_MEMBER_JOINED_NFY));
	//rc = g_pApp->Send( this->GetHandle(), &packet );
	app->UserBroadcastothers(&packet2, this);
}
//--------------------------------------------------------------------------------------//
//		Leave Party
//--------------------------------------------------------------------------------------//
void CClientSession::SendPartyLeaveReq(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- leave party request --- \n");

	CNtlPacket packet(sizeof(sGU_PARTY_LEAVE_RES));
	sGU_PARTY_LEAVE_RES * res = (sGU_PARTY_LEAVE_RES *)packet.GetPacketData();

	res->wOpCode = GU_PARTY_LEAVE_RES;
	res->wResultCode = GAME_SUCCESS;

	packet.SetPacketLen( sizeof(sGU_PARTY_DISBAND_RES) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );


	CNtlPacket packet2(sizeof(sGU_PARTY_MEMBER_LEFT_NFY));
	sGU_PARTY_MEMBER_LEFT_NFY * sPacket2 = (sGU_PARTY_MEMBER_LEFT_NFY *)packet2.GetPacketData();
	sPacket2->wOpCode = GU_PARTY_MEMBER_LEFT_NFY;
	sPacket2->hMember = this->GetTargetSerialId();

	packet2.SetPacketLen( sizeof(sGU_PARTY_MEMBER_LEFT_NFY));
	rc = g_pApp->Send( this->GetHandle(), &packet2);

	app->UserBroadcastothers(&packet2, this);
}



//--------------------------------------------------------------------------------------//
//		Execute trigger object
//--------------------------------------------------------------------------------------//
void CClientSession::SendExcuteTriggerObject(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- SendExcuteTriggerObject --- \n");

	sUG_TS_EXCUTE_TRIGGER_OBJECT * req = (sUG_TS_EXCUTE_TRIGGER_OBJECT*)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sGU_TS_EXCUTE_TRIGGER_OBJECT_RES));
	sGU_TS_EXCUTE_TRIGGER_OBJECT_RES * res = (sGU_TS_EXCUTE_TRIGGER_OBJECT_RES *)packet.GetPacketData();

	res->wOpCode = GU_TS_EXCUTE_TRIGGER_OBJECT_RES;
	res->wResultCode = GAME_SUCCESS;
	res->hTriggerObject = req->hTarget;

	printf("SOURCE: %i TARGET: %i EVTGENTYPE: %i ", req->hSource, req->hTarget, req->byEvtGenType);

	packet.SetPacketLen( sizeof(sGU_TS_EXCUTE_TRIGGER_OBJECT_RES) );
	app->UserBroadcastothers(&packet, this);
	int rc = g_pApp->Send( this->GetHandle(), &packet );
}
//--------------------------------------------------------------------------------------//
//		Character bind to world
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharBindReq(CNtlPacket * pPacket, CGameServer * app)
{
	printf("--- UG_CHAR_BIND_REQ --- \n");

	sUG_CHAR_BIND_REQ * req = (sUG_CHAR_BIND_REQ*)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sGU_CHAR_BIND_RES));
	sGU_CHAR_BIND_RES * res = (sGU_CHAR_BIND_RES *)packet.GetPacketData();

	app->db->prepare("CALL CharBind (?,?, @currentWorldID)");
	app->db->setInt(1, this->characterID);
	app->db->setInt(2, req->bindObjectTblidx);
	app->db->execute();
	app->db->execute("SELECT @currentWorldID");
	app->db->fetch(); 

	res->wOpCode = GU_CHAR_BIND_RES;
	res->wResultCode = GAME_SUCCESS;
	res->byBindType = DBO_BIND_TYPE_FIRST;
	res->bindObjectTblidx = req->bindObjectTblidx;

	res->bindWorldId = app->db->getInt("@currentWorldID");

	printf("bindWorldId: %i  bindObjectTblidx:%i", app->db->getInt("@currentWorldID"), req->bindObjectTblidx);

	packet.SetPacketLen( sizeof(sGU_CHAR_BIND_RES) );
	int rc = g_pApp->Send( this->GetHandle(), &packet );
}

