#pragma once

#include "NtlSfx.h"
#include "NtlPacketEncoder_RandKey.h"
#include "NtlFile.h"
#include "mysqlconn_wrapper.h"

//#include "NtlPacketUT.h"
//#include "NtlPacketTU.h"
#include "NtlPacketUG.h"
#include "NtlPacketGU.h"
#include "ResultCode.h"

#include "GsFunctions.h"

#include "NtlItem.h"
#include "Battle.h"
// LOAD TABLES
#include "NPCTable.h"
#include "TableContainer.h"
#include "WorldTable.h"
#include "MobTable.h"
#include "PCTable.h"
#include "SkillTable.h"
#include "HTBSetTable.h"
#include "SpawnTable.h"
#include "TableFileNameList.h"
#include "WorldMapTable.h"
#include "WorldPathTable.h"
#include "ObjectTable.h"
// END TABLES
#include "Vector.h"

#include "NtlBitFlagManager.h"

#include <iostream>
#include <map>
#include <list>

enum APP_LOG
{
	PRINT_APP = 2,
};
enum GAME_SESSION
{
	SESSION_CLIENT,
};
struct sSERVERCONFIG
{
	CNtlString		strClientAcceptAddr;
	WORD			wClientAcceptPort;
};

const DWORD					MAX_NUMOF_GAME_CLIENT = 3;
const DWORD					MAX_NUMOF_SERVER = 1;
const DWORD					MAX_NUMOF_SESSION = MAX_NUMOF_GAME_CLIENT + MAX_NUMOF_SERVER;

class CGameServer;
class CTableContainer;
class CTableFileNameList;
class CNtlBitFlagManager;

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
class CClientSession : public CNtlSession
{
public:

	CClientSession(bool bAliveCheck = false, bool bOpcodeCheck = false)
		:CNtlSession( SESSION_CLIENT )
	{
		SetControlFlag( CONTROL_FLAG_USE_SEND_QUEUE );

		if( bAliveCheck )
		{
			SetControlFlag( CONTROL_FLAG_CHECK_ALIVE );
		}

		if( bOpcodeCheck )
		{
			SetControlFlag( CONTROL_FLAG_CHECK_OPCODE );
		}

		SetPacketEncoder( &m_packetEncoder );
	}

	~CClientSession();


public:
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);

	//
	unsigned int				GetavatarHandle() { return avatarHandle; }
	unsigned int				GetTargetSerialId() { return uiTargetSerialId; }
	unsigned int				GetTargetNpcSerialId() { return uiNPCSerialId; }
	unsigned int				GetCharacterId() { return characterID; }
	int							GetCAccountId() { return accountID; }
	std::string					GetCharName() { return charName; }
	//
	//Client Packet functions
	void						SendGameEnterReq(CNtlPacket * pPacket, CGameServer * app);
	void						SendAvatarCharInfo(CNtlPacket * pPacket, CGameServer * app);
//	void						SendAvatarItemInfo(CNtlPacket * pPacket, CGameServer * app);
//	void						SendAvatarSkillInfo(CNtlPacket * pPacket, CGameServer * app);
	void						SendAvatarInfoEnd(CNtlPacket * pPacket);
	void						SendAuthCommunityServer(CNtlPacket * pPacket, CGameServer * app);

	void						SendWorldEnterReq(CNtlPacket * pPacket, CGameServer * app);
//	void						SendPlayerCreate(CNtlPacket * pPacket, CGameServer * app);
	void						SendNpcCreate(CNtlPacket * pPacket, CGameServer * app);
	void						SendMonsterCreate(CNtlPacket * pPacket, CGameServer * app);
	void						SendEnterWorldComplete(CNtlPacket * pPacket);

	void						SendCharReadyReq(CNtlPacket * pPacket, CGameServer * app);
	void						SendTutorialHintReq(CNtlPacket * pPacket, CGameServer * app);
	void						SendCharReady(CNtlPacket * pPacket);
	void						SendCharMove(CNtlPacket * pPacket, CGameServer * app);
	void						SendCharDestMove(CNtlPacket * pPacket, CGameServer * app);
	void						SendCharMoveSync(CNtlPacket * pPacket, CGameServer * app);
	void						SendCharChangeHeading(CNtlPacket * pPacket, CGameServer * app);
	void						SendCharJump(CNtlPacket * pPacket, CGameServer * app);
	void						SendCharChangeDirOnFloating(CNtlPacket * pPacket, CGameServer * app);
	void						SendCharFalling(CNtlPacket * pPacket, CGameServer * app);
	void						SendCharExitReq(CNtlPacket * pPacket, CGameServer * app);
	void						SendGameLeaveReq(CNtlPacket * pPacket, CGameServer * app);
	void						RecvServerCommand(CNtlPacket * pPacket, CGameServer * app);
	void						SendUpdateCharSpeed(float fSpeed, CGameServer * app);
	void						SendCharTargetSelect(CNtlPacket * pPacket);
	void						SendCharTargetFacing(CNtlPacket * pPacket);
	void						SendCharTargetInfo(CNtlPacket * pPacket);
	void						SendCharSitDown(CNtlPacket * pPacket, CGameServer * app);
	void						SendCharStandUp(CNtlPacket * pPacket, CGameServer * app);
	void						SendCharToggleFighting(CNtlPacket * pPacket, CGameServer * app);
	void						SendCharFollowMove(CNtlPacket * pPacket, CGameServer * app);
	void						SendExcuteTriggerObject(CNtlPacket * pPacket, CGameServer * app);
	void						SendCharBindReq(CNtlPacket * pPacket, CGameServer * app);

	void						SendGuildCreateReq(CNtlPacket * pPacket, CGameServer * app);

	void						SendPartyInviteReq(CNtlPacket * pPacket, CGameServer * app);
	void						SendCreatePartyReq(CNtlPacket * pPacket, CGameServer * app);
	void						SendDisbandPartyReq(CNtlPacket * pPacket, CGameServer * app);
	void						SendPartyLeaveReq(CNtlPacket * pPacket, CGameServer * app);
	void						SendPartyResponse(CNtlPacket * pPacket, CGameServer * app);

	void						SendCharMailStart(CNtlPacket * pPacket, CGameServer * app);
	//
	//Game Server functions
	sPC_SUMMARY					GetPcSummary(sPC_SUMMARY * sPcDataSummary, int characterID, CGameServer * app);
	sPC_BRIEF					GetPcBrief(sPC_BRIEF * sPcBriefSummary, int characterID, CGameServer * app);
	sPC_SHAPE					GetPcShape(sPC_SHAPE * sPcShapeSummary, int characterID, CGameServer * app);
	sGU_OBJECT_CREATE			characterspawnInfo;
	//

	typedef struct _SBattleData
	{
	RwUInt32	uiSerialId;
	RwUInt32	uiTargetSerialId;
	bool			bAttackMode;
	DWORD			dwCurrTime;
	}SBattleData;

typedef std::list<SBattleData*> ListAttackBegin;
ListAttackBegin m_listAttackBegin;

private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
	unsigned int avatarHandle;
	unsigned int uiTargetSerialId;
	unsigned int uiNPCSerialId;
	int accountID;
	int characterID;
	std::string					charName;
};

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//

class CGameSessionFactory : public CNtlSessionFactory
{
public:

	CNtlSession * CreateSession(SESSIONTYPE sessionType)
	{
		CNtlSession * pSession = NULL;
		switch( sessionType )
		{
		case SESSION_CLIENT: 
			{
				pSession = new CClientSession;
			}
			break;

		default:
			break;
		}

		return pSession;
	}
};

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//

class CGameServer : public CNtlServerApp
{
public:
	int					OnInitApp()
	{
		m_nMaxSessionCount = MAX_NUMOF_SESSION;
		m_pSessionFactory = new CGameSessionFactory;
		if( NULL == m_pSessionFactory )
		{
			return NTL_ERR_SYS_MEMORY_ALLOC_FAIL;
		}
		return NTL_SUCCESS;
	}
	int					OnCreate()
	{
		int rc = NTL_SUCCESS;
		rc = m_clientAcceptor.Create(	m_config.strClientAcceptAddr.c_str(), m_config.wClientAcceptPort, SESSION_CLIENT,MAX_NUMOF_GAME_CLIENT, 5, 2, MAX_NUMOF_GAME_CLIENT );
		if ( NTL_SUCCESS != rc )
		{
			return rc;
		}
		rc = m_network.Associate( &m_clientAcceptor, true );
		if( NTL_SUCCESS != rc )
		{
			return rc;
		}
		return NTL_SUCCESS;
	}
	void				OnDestroy()
	{
	}
	int					OnCommandArgument(int argc, _TCHAR* argv[])
	{
		return NTL_SUCCESS;
	}
	int					OnConfiguration(const char * lpszConfigFile)
	{
		CNtlIniFile file;
		int rc = file.Create( lpszConfigFile );
		if( NTL_SUCCESS != rc )
		{
			return rc;
		}
		if( !file.Read("Game Server", "Address", m_config.strClientAcceptAddr) )
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if( !file.Read("Game Server", "Port",  m_config.wClientAcceptPort) )
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		return NTL_SUCCESS;
	}
	int					OnAppStart()
	{

		if(CreateTableContainer(2))
		{
			return NTL_SUCCESS;
		}else{
		printf("FAILED LOADING TABLES !!! \n");
		return NTL_SUCCESS;
		}
	}
	void				Run()
	{
		DWORD dwTickCur, dwTickOld = ::GetTickCount();

		while( IsRunnable() )
		{		
			dwTickCur = ::GetTickCount();
			if( dwTickCur - dwTickOld >= 10000 )
			{
			//	NTL_PRINT(PRINT_APP, "Char Server Run()");
				dwTickOld = dwTickCur;
			}		
		}
	}
private:
	CNtlAcceptor				m_clientAcceptor;
	CNtlLog  					m_log;
	sSERVERCONFIG				m_config;
public:
	MySQLConnWrapper *			db;

public:
	CTableContainer	*			g_pTableContainer;
	bool						CreateTableContainer(int byLoadMethod);
	bool						SpawnNpcs();
	bool						SpawnMobs();

	bool						AddUser(const char * lpszUserID, CClientSession * pSession)
	{
		if( false == m_userList.insert( USERVAL(CNtlString(lpszUserID), pSession)).second )
		{
			return false;
		}
		return true;		
	}
	void						RemoveUser(const char * lpszUserID)
	{
		m_userList.erase( CNtlString(lpszUserID) );
	}
	bool						FindUser(const char * lpszUserID)
	{
		USERIT it = m_userList.find( CNtlString(lpszUserID) );
		if( it == m_userList.end() )
			return false;

		return true;
	}
	void						UserBroadcast(CNtlPacket * pPacket)
	{
		for( USERIT it = m_userList.begin(); it != m_userList.end(); it++ )
		{
			it->second->PushPacket( pPacket );
		}
	}
	void						UserBroadcastothers(CNtlPacket * pPacket, CClientSession * pSession)
	{
		for( USERIT it = m_userList.begin(); it != m_userList.end(); it++ )
		{
			if(pSession->GetavatarHandle() != it->second->GetavatarHandle())
			it->second->PushPacket( pPacket );
		}
	}
	void						UserBroadcasFromOthers(eOPCODE_GU opcode, CClientSession * pSession)
	{
		if(opcode == GU_OBJECT_CREATE)
		{
			for( USERIT it = m_userList.begin(); it != m_userList.end(); it++ )
			{
				if(pSession->GetavatarHandle() != it->second->GetavatarHandle())
				{
					CNtlPacket packet(sizeof(sGU_OBJECT_CREATE));
					sGU_OBJECT_CREATE * sPacket = (sGU_OBJECT_CREATE *)packet.GetPacketData();
					memcpy(sPacket, &it->second->characterspawnInfo, sizeof(sGU_OBJECT_CREATE));
					sPacket->handle = it->second->GetavatarHandle();

					packet.SetPacketLen( sizeof(sGU_OBJECT_CREATE) );
					pSession->PushPacket(&packet);
					//g_pApp->Send( this->GetHandle(), &packet );
					
				//it->second->PushPacket( pPacket );
				}
			}
		}
	}


	typedef std::map<CNtlString, CClientSession*> USERLIST;
	typedef USERLIST::value_type USERVAL;
	typedef USERLIST::iterator USERIT;

	USERLIST					m_userList;


};

